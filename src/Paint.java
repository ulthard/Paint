import java.awt.Cursor;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.FlowLayout;
import java.awt.Color;
import java.awt.event.MouseMotionListener;
import java.io.FileNotFoundException;

import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;
import java.awt.BorderLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JToggleButton;

public class Paint extends JFrame implements Donnees {

    private static final long serialVersionUID = -6899734505967857312L;
    private JToggleButton jToggleButtonCrayon;
    private JToggleButton jToggleButtonGomme;
    private JToggleButton jToggleButtonFleche;
    private JTextField jTextFieldTaille;
    private JComboBox<String> jComboBoxCouleur;
    private JComboBox<String> jComboBoxForme;
    private PanneauCentre panneauCentre;

    public Paint(Cursor cursor) {
        super("Paint");
        this.initialiseMenu();
        this.initialiseNord();
        this.initialiseSud();
        this.initialiseCentre();
        this.initialiseWindow(cursor);
    }

    private void initialiseMenu() {
        JMenuBar jMenuBar = new JMenuBar();
        this.setJMenuBar(jMenuBar);

        JMenu jMenuFichier = new JMenu("Fichier");
        JMenuItem jMenuItemCharger = new JMenuItem("Charger");
        jMenuItemCharger.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                try {
                    Fichiers.load("./src/data/save.txt", Donnees.dessins);
                    panneauCentre.repaint();
                } catch (FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });
        jMenuFichier.add(jMenuItemCharger);

        JMenuItem jMenuItemSauvegarder = new JMenuItem("Sauvegarder");
        jMenuItemSauvegarder.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                try {
                    Fichiers.save("./src/data/save.txt", Donnees.dessins);
                } catch (FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });
        jMenuFichier.add(jMenuItemSauvegarder);

        JMenuItem jMenuItemQuitter = new JMenuItem("Quitter");
        jMenuItemQuitter.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                System.exit(0);
            }
        });
        jMenuFichier.add(jMenuItemQuitter);
        jMenuBar.add(jMenuFichier);

        JMenu jMenuPropos = new JMenu("À propos");
        jMenuPropos.addMenuListener(new MenuListener() {
            @Override
            public void menuSelected(MenuEvent arg0) {
                JOptionPane.showMessageDialog(null, "Réalisation de JETON Alex le 01/01/2018", "Informations",
                        JOptionPane.INFORMATION_MESSAGE);
            }

            @Override
            public void menuCanceled(MenuEvent arg0) {
                // TODO Auto-generated method stub
            }

            @Override
            public void menuDeselected(MenuEvent arg0) {
                // TODO Auto-generated method stub
            }
        });
        jMenuBar.add(jMenuPropos);
    }

    private void initialiseNord() {
        JPanel jPanel = new JPanel();
        jPanel.setLayout(new FlowLayout());

        JLabel jLabelCouleur = new JLabel("Couleur : ");
        jPanel.add(jLabelCouleur);
        this.jComboBoxCouleur = new JComboBox<String>(Donnees.couleurs);
        jPanel.add(this.jComboBoxCouleur);

        JLabel jLabelForme = new JLabel("Forme : ");
        jPanel.add(jLabelForme);
        this.jComboBoxForme = new JComboBox<String>(Donnees.formes);
        jPanel.add(this.jComboBoxForme);

        JLabel jLabelTaille = new JLabel("Taille : ");
        jPanel.add(jLabelTaille);
        this.jTextFieldTaille = new JTextField(20);
        jPanel.add(this.jTextFieldTaille);

        this.jToggleButtonCrayon = new JToggleButton(new ImageIcon("./src/data/crayon.png"));
        jPanel.add(this.jToggleButtonCrayon);

        this.jToggleButtonGomme = new JToggleButton(new ImageIcon("./src/data/gomme.png"));
        jPanel.add(this.jToggleButtonGomme);

        this.jToggleButtonFleche = new JToggleButton(new ImageIcon("./src/data/fleche.png"));
        jPanel.add(this.jToggleButtonFleche);

        this.add(jPanel, BorderLayout.NORTH);
    }

    private void initialiseSud() {
        /* TODO : IMAGE */
        PanneauSud panneauSud = new PanneauSud();
        panneauSud.setLayout(new FlowLayout());
        JButton jButtonEffacer = new JButton("Effacer tout");
        jButtonEffacer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                Donnees.dessins.clear();
                panneauCentre.repaint();
            }
        });
        panneauSud.add(jButtonEffacer);
        JButton jButtonRevenir = new JButton("Revenir en arrière");
        jButtonRevenir.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                Donnees.dessins.remove(Donnees.dessins.size()-1);
                panneauCentre.repaint();
            }
        });
        panneauSud.add(jButtonRevenir);
        this.add(panneauSud, BorderLayout.SOUTH);
    }

    private void initialiseCentre() {
        this.panneauCentre = new PanneauCentre();
        this.panneauCentre.setBackground(Color.WHITE);
        this.panneauCentre.addMouseListener(new CentreMouseListener());
        this.panneauCentre.addMouseMotionListener(new CentreMouseMotionListener());
        this.add(this.panneauCentre, BorderLayout.CENTER);
    }

    private void initialiseWindow(Cursor cursor) {
        this.setCursor(cursor);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        this.setBounds(0,0, toolkit.getScreenSize().width, toolkit.getScreenSize().height);
        this.setVisible(true);
    }

    public int choixDessins(int abscisse,int ordonnee)
	{
		int var = -1;
		if(Donnees.dessins.isEmpty()==false) {
			for(int i = 0;i<Donnees.dessins.size();i++) {
                if
                ( 
                    (abscisse >= Donnees.dessins.get(i).getAbscisse() - Donnees.dessins.get(i).getTaille()/2 
                        && abscisse <= Donnees.dessins.get(i).getAbscisse() + Donnees.dessins.get(i).getTaille())
                    &&
                    (ordonnee >= Donnees.dessins.get(i).getOrdonnee() - Donnees.dessins.get(i).getTaille()/2 
                        && ordonnee <= Donnees.dessins.get(i).getOrdonnee() + Donnees.dessins.get(i).getTaille())
                )
                    var = i;
			}
		}
		
		return var;
    }
    
    class CentreMouseMotionListener implements MouseMotionListener {

        @Override
        public void mouseDragged(MouseEvent arg0) {
            if (jToggleButtonFleche.isSelected()) {
                if(! Donnees.dessins.isEmpty()) {
                    int varDessin = choixDessins(arg0.getX(),arg0.getY());
                    if(varDessin != -1){
                        Donnees.dessins.get(varDessin).setAbscisse(arg0.getX());
                        Donnees.dessins.get(varDessin).setOrdonnee(arg0.getY());
                        panneauCentre.repaint();
                    }
                }
            }
        }

        @Override
        public void mouseMoved(MouseEvent arg0) {
            // TODO Auto-generated method stub
        }
        
    }

    class CentreMouseListener implements MouseListener {

        @Override
        public void mouseClicked(MouseEvent arg0) {
            if (jToggleButtonCrayon.isSelected() && jToggleButtonGomme.isSelected())
                JOptionPane.showMessageDialog(null, "Vous avez selectionné le crayon ET la gomme.", "Warning",
                        JOptionPane.WARNING_MESSAGE);

            if (jToggleButtonCrayon.isSelected() && !jToggleButtonGomme.isSelected()) {
                String varJTextFieldTaille = jTextFieldTaille.getText();
                if(varJTextFieldTaille.equals(""))
                    varJTextFieldTaille = "100";
                
                Color color ;
                switch(jComboBoxCouleur.getSelectedItem().toString()) {
                    case "rouge":
                        color = new Color(255,0,0);
                        break;
                    case "bleu":
                        color = new Color(0,0,255);
                        break;
                    case "vert":
                        color = new Color(0,255,0);
                        break;
                    default:
                        color = new Color(0,255,0);
                }

                Donnees.dessins.add(new Dessin(Integer.parseInt(varJTextFieldTaille),arg0.getX(),arg0.getY(),
                    jComboBoxForme.getSelectedItem().toString(),color));
                
                panneauCentre.repaint();
            }

            if (! jToggleButtonCrayon.isSelected() && jToggleButtonGomme.isSelected()) {
                Donnees.dessins.add(new Dessin(100,arg0.getX(),arg0.getY(),"carre",Color.WHITE));
                panneauCentre.repaint();
            }
        }

        @Override
        public void mouseEntered(MouseEvent arg0) {
            // TODO Auto-generated method stub

        }

        @Override
        public void mouseExited(MouseEvent arg0) {
            // TODO Auto-generated method stub

        }

        @Override
        public void mousePressed(MouseEvent arg0) {
            // TODO Auto-generated method stub

        }

        @Override
        public void mouseReleased(MouseEvent arg0) {
            // TODO Auto-generated method stub

        }
    }

}