import javax.swing.JPanel;
import java.awt.Graphics;

public class PanneauCentre extends JPanel {
    
    private static final long serialVersionUID = 1L;

    public PanneauCentre() {
        super();
    }
    
    public void paintComponent(Graphics g)
	{
        super.paintComponent(g);
		if(! Donnees.dessins.isEmpty()) 
		{
            for(Dessin i : Donnees.dessins) {
                if(i.getForme().equals("cercle")) {
                    g.setColor(i.getCouleur());
                    g.fillOval(i.getAbscisse(),i.getOrdonnee(),i.getTaille()/2,i.getTaille()/2);
                }
                else {
                    g.setColor(i.getCouleur());
                    g.fillRect(i.getAbscisse(),i.getOrdonnee(),i.getTaille()/2,i.getTaille()/2);
                }
            }
        }
	}
}