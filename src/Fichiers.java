import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Fichiers {
    
    public static final void save(String fileName, ArrayList<Dessin> dessins) throws FileNotFoundException {
        PrintWriter pw = new PrintWriter(new FileOutputStream(fileName));
        for (Dessin i : dessins)
            pw.println(i.saveData());
        pw.close();
    }

    public static final void load(String fileName, ArrayList<Dessin> dessins) throws FileNotFoundException {
        dessins.clear();
        Scanner s = new Scanner(new File(fileName));
        while (s.hasNext()){
            String[] varString = s.next().split(",");
            dessins.add(new Dessin(Integer.parseInt(varString[0]), Integer.parseInt(varString[3]), Integer.parseInt(varString[4]),
                varString[1], new Color(Integer.parseInt(varString[2]))));
        }
        s.close();
    }

}
